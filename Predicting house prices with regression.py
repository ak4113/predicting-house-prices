
# coding: utf-8

# # Predicting house prices using regression

# In[24]:

import graphlab
graphlab.canvas.set_target('ipynb')
sales = graphlab.SFrame('home_data.gl')


# In[3]:

sales


# In[4]:

sales.show()


# # Find average house price of zip 98039

# In[4]:

sales.show(view='BoxWhisker Plot', x='zipcode', y='price')


# In[5]:

tophouses = sales[sales['zipcode']=='98039']


# In[6]:

print tophouses['price'].mean()


# # Find fraction of houses with sqft_living > 2000 but <= 4000

# In[7]:

houseslice = sales[(sales['sqft_living'] > 2000) & (sales['sqft_living'] <= 4000)]


# In[54]:

houseslice.show()


# In[57]:

print "Houses with >2000sqft and <=4000sqft: " + str(houseslice.num_rows()) # this is right
print "Total houses in dataset:" + str(sales.num_rows()) # this is right
fraction = 1.0 * houseslice.num_rows() / sales.num_rows() # this returns 0!
print fraction
print "Fraction of houses with >2000sqft and <=4000sqft: " + str(fraction)


# # Build regression model from advanced features
# ## Define feature sets

# In[21]:

my_features = ['bedrooms', 'bathrooms', 'sqft_living', 'sqft_lot', 'floors', 'zipcode']
advanced_features = ['bedrooms', 'bathrooms', 'sqft_living', 'sqft_lot', 'floors', 'zipcode', 'condition', 'grade', 
                     'waterfront', 'view', 'sqft_above', 'sqft_basement', 'yr_built', 'yr_renovated', 'lat', 'long', 
                     'sqft_living15','sqft_lot15']


# ## Setup training data and test data

# In[22]:

train_data,test_data = sales.random_split(.8,seed=0)


# In[26]:

features_model = graphlab.linear_regression.create(train_data,target='price',features=my_features,validation_set=None)


# In[27]:

advfeatures_model = graphlab.linear_regression.create(train_data,target='price',features=advanced_features,validation_set=None)


# ## Evaluate both models versus test data

# In[31]:

print features_model.evaluate(test_data)
print advfeatures_model.evaluate(test_data)


# In[ ]:



